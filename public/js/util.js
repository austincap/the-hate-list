var MemewarUtil = function(sounds, socket, game){
  this.sounds = sounds;
  this.socket = socket;
  this.game = game;
};
MemewarUtil.prototype.getRandomInt = function(min, max){
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};
MemewarUtil.prototype.tweenTint = function(obj, startColor, endColor, time){
  var colorBlend = {step: 0};  // create an object to tween with our step value at 0
  var colorTween = game.add.tween(colorBlend).to({step: 100}, time);  // create the tween on this object and tween its step property to 100 
  // run the interpolateColor function every time the tween updates, feeding it the    
  // updated value of our tween each time, and set the result as our tint    
  colorTween.onUpdateCallback(function() {     
    obj.tint = Phaser.Color.interpolateColor(startColor, endColor, 100, colorBlend.step);       
  });
  obj.tint = startColor;  // set the object to the start color straight away  
  colorTween.start();
  colorTween.loop();
};
MemewarUtil.prototype.messageOverlay = function(message, timeToShow){
  var textX = playerManager.clientPlayerSprite.x;
  var textY = playerManager.clientPlayerSprite.y;   
  var style = { font: '12pt Arial', fill: '#dfe09d', align: 'left', wordWrap: true, wordWrapWidth: 300, maxLines: 6};
  var textObject = game.add.text(textX, textY+this.getRandomInt(-100,100), message, style);
  var tween = game.add.tween(textObject).to({alpha:0},3000, "Linear",true);
  tween.onComplete.add(function(){
    textObject.destroy();
  });
};
MemewarUtil.prototype.drawLinkBlocks = function(game, linkblocksIterable){
  console.log('draw LinkBlocks');
  for(var i = 0; i < Object.keys(linkblocksIterable).length; i++){
    //parse each element of the array as JSON
    //singleLinkblock.type refers to the data sent from the server, not the sprite, so we don't use .blockType
    var singleLinkblock = linkblocksIterable[i];
    var theLinkBlock = linkBlocks.createLinkBlock(parseInt(singleLinkblock.x), 
      parseInt(singleLinkblock.y), 25,25, singleLinkblock.type, 
      singleLinkblock.url, parseInt(singleLinkblock.upvotes), 
      String(singleLinkblock.blockID))
    linkBlocksGroup.add(theLinkBlock);
    var picLocation = singleLinkblock.url;
    if (picLocation.substr(0, 6) === "public" || picLocation.substr(0, 6) === "picsup"){
      if(picLocation.slice(-3)=="gif"){
        var imageSprite = game.add.sprite(singleLinkblock.x-12, singleLinkblock.y-12, picLocation.slice(7).substr(13,14));
        imageSprite.scale.setTo(0.32,0.32);
        imageSprite.data.isGif = true;
        game.physics.arcade.enable(imageSprite);
        imageSprite.body.enable = true;
        imageSprite.inputEnabled = true;
        imageSprite.anchor.setTo(0);
      } else{
        var imageSprite = game.add.sprite(singleLinkblock.x-12, singleLinkblock.y-12, picLocation.slice(7).substr(13,14));
        imageSprite.scale.setTo(0.16,0.16);
        game.physics.arcade.enable(imageSprite);
        imageSprite.body.enable = true;
        imageSprite.inputEnabled = true;
        imageSprite.anchor.setTo(0);
      }
      imageGroup.add(imageSprite);
    }
    
    //add stuff to dataStream if not empty url and at least 4 upvotes
    if(singleLinkblock.url != ("" || "u" || "l" || "r" || "d") && singleLinkblock.upvotes >= 4){ dataStreamAnimation(singleLinkblock.url);}    
    //add identicons to every block that actually requires an identicon (regular_linkblocks and textblocks are covered in their js files)
    if(singleLinkblock.type != "messageblock" &&
      singleLinkblock.type != "regular_linkblock" &&
      singleLinkblock.type != "wallblock" &&
      singleLinkblock.type != "textblock" &&
      singleLinkblock.type != "shuntblock" &&
      singleLinkblock.type != "labelblock" && 
      singleLinkblock.type != "viral_linkblock" &&
      singleLinkblock.type != "organic_linkblock" &&
      singleLinkblock.type != "verse_portal"){
      //console.log(singleLinkblock.x, singleLinkblock.y, String(singleLinkblock.blockID));
      var identiconSprite = this.game.add.image(singleLinkblock.x, singleLinkblock.y, singleLinkblock.blockID);
      identiconSprite.anchor.setTo(0.5,0.7);
      var identicon = {sprite: identiconSprite, blockID: singleLinkblock.blockID};
      linkBlocks.identiconPicArray.push(identicon);
      if(singleLinkblock.type == "mine_linkblock"){
        identiconSprite.alpha = 0.05;
      }
      
      if(singleLinkblock.type == "aggro_linkblock"){        
        turretBlocksGroup.add(theLinkBlock);        
      }
    }
  }
};
MemewarUtil.prototype.blockHealthChanged = function(blockID, upDownOrDelete){
  //get linkBlock by blockID
  var thisLinkBlock = linkBlocksGroup.children.filter(linkBlock => linkBlock.blockID==blockID)[0];
  if(!thisLinkBlock) //must be turret block
    thisLinkBlock = turretBlocksGroup.children.filter(x => x.blockID === blockID)[0];
  if(upDownOrDelete==4){ //WHEN BLOCK IS EDITED, I KNOW THIS IS STUPID, WE HAVE TO MAKE THIS INTO A MORE GENERAL BLOCKCHANGED FUNCTION    
    thisLinkBlock.url = blockID.url;
    return;
  }
  var mvstyle = {font: 'bold 10pt Arial', fill: '#5bd9e6', align: 'left', wordWrap: true, wordWrapWidth: 100, maxLines:2 };
  if(thisLinkBlock.blockType=='verse_portal'){
    if(upDownOrDelete==1){ 
      thisLinkBlock.health++;
      this.sounds.superpickup.play();
      game.add.tween(thisLinkBlock).to({tint: 0x00ff00}, 160, Phaser.Easing.Exponential.Out, true, 0, 0, true);
    }else if(upDownOrDelete==-1){
      thisLinkBlock.health--;
      this.sounds.damage.play();
      game.add.tween(thisLinkBlock).to({tint: 0xff0000}, 160, Phaser.Easing.Exponential.Out, true, 0, 0, true);
    }else if(upDownOrDelete==0){
      var explosionSprite = game.add.sprite(thisLinkBlock.body.x-20, thisLinkBlock.body.y-20, 'explosion');
      explosionSprite.animations.add('explode', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 20, false, true);
      explosionSprite.animations.play('explode', 20, false, true); 
      thisLinkBlock.destroy();
      util.messageOverlay("Post ID "+thisLinkBlock.blockID+" ZUCCED!", 1000);
      this.sounds.zucced.play();
    }else if(upDownOrDelete==-3){
      thisLinkBlock.health-=3;
      this.sounds.damage.play();
      game.add.tween(thisLinkBlock).to({tint: 0xff0000}, 160, Phaser.Easing.Exponential.Out, true, 0, 0, true);
    }
  }else{
    thisLinkBlock.blockHealthChanged(upDownOrDelete);
  }
};
MemewarUtil.prototype.drawConnectingLines = function(connectingLinesIterable){
  for(var i = 0; i < Object.keys(connectingLinesIterable).length; i++){
    var line=game.add.graphics(0,0);
    line.beginFill();
    line.lineStyle(1, 0x4dffdb, 1);
    line.moveTo(connectingLinesIterable[i].x1, connectingLinesIterable[i].y1);
    line.lineTo(connectingLinesIterable[i].x2, connectingLinesIterable[i].y2);
    line.endFill();
    this.tweenTint(line, 0xff0000, 0x4dffdb, 2000); // tween the tint of sprite from red to turquoise over 2 seconds (2000ms)
    game.world.sendToBack(line);
  }
  console.log('lines drawn');
};
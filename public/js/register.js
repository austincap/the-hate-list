$(function(){
	var formContext = {
		isUsernameUnique: false,
		isEmailValid: false
	}
	
	function validate(){
		return validatePassword() && validateUsername() && validateEmail();
	};
	function validatePassword(){
		var password = $('#password').val();
		if(password.length >= 8){
			$('#password').parent().find('.helper-label').hide();	
		} else {			
			$('#password').parent().find('.helper-label').show();	
			return false;
		}
	};
	function validateRepeatPassword(){
		var password = $('#password').val();
		var repeat = $('#repeat-password').val();
		if(password !== repeat){
			$('#repeat-password').parent().find('.helper-label').show();
			return false;
		} else {
			$('#repeat-password').parent().find('.helper-label').hide();
		}
	};
	function validateUsername(){
		if(!formContext.isUsernameUnique){
			$('#username').parent().find('.helper-label').show();	
			return false;
		} else {
			$('#username').parent().find('.helper-label').hide();	
		}
	};
	function validateEmail(){
		if(!formContext.isEmailValid){
			$('#email').parent().find('.helper-label').show();	
			return false;
		} else {
			$('#email').parent().find('.helper-label').hide();	
		}
	};
	$('#password').change(function(){
		validatePassword();
		validateRepeatPassword();
	});	
	$('#repeat-password').change(validateRepeatPassword);	
	$('#username').change(function(){
		$.post('isUserUnique', {username: $(this).val()}).done(function(response){
			formContext.isUsernameUnique = response.data;			
		}).fail(function(){
			formContext.isUsernameUnique = false;
		}).always(validateUsername);
		console.log('need to make a request to server to check user name uniqueness.');
		
	});
	$('#email').change(function(){
		formContext.isEmailValid = $('#email:valid').length > 0;
		validateEmail();		
	})
	$('#register-form').submit(function(event){
		if(!validate()){			
			return false;
		}
	})
});
//var socket = io.connect("http://localhost:4000");
var socket = io.connect("http://159.203.169.144:4000");

function loadHateeDiscussionData(){
	console.log(window.location.href.slice(-24));
	socket.emit("getAllComments", window.location.href.slice(-24));
}

loadHateeDiscussionData();

function returnToMain(){
	window.location.href = "/index.html";
}

function showCommentEntryOverlay(type){
	//2=pro, 3=con, 4=detail
	$("#commentEntryOverlay").show();
	$("#commentEntryOverlay").attr("data-commentType", type);
}

function addNewComment(){
	var newCommentDataPackage = {
		hateeId: window.location.href.slice(-24),
		content: $("#commentTextInput").val(),
		commentType: parseInt($("#commentEntryOverlay").attr("data-commentType"))
	};
	socket.emit("newComment", newCommentDataPackage);
	$("#commentEntryOverlay").hide();
	location.reload();
}

function upvoteComment(element){
	console.log('upvote this commentID', $(element).parent().attr("id"));
	socket.emit("voteComment", {upOrDown:1, commentID:$(element).parent().attr("id")});
	location.reload();
}

function downvoteComment(element){
	console.log('downvote this commentID', $(element).parent().attr("id"));
	socket.emit("voteComment", {upOrDown:0, commentID:$(element).parent().attr("id")});
	location.reload();
}

socket.on("heresYourPersonName", function(hateeData){
	$("#personNameDiscussion").text(hateeData.personName);
	$("#personHatersDiscussion").text(hateeData.haterCount);
});

socket.on("heresYourComments", function(commentData){
	//var reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
	for(var i=0; i<commentData.length; i++){
		// var newCommentData = commentData[i].replace(/(<a href=")?((https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)))(">(.*)<\/a>)?/gi, function () {
		//     return '<a href="' + arguments[2] + '">' + (arguments[7] || arguments[2]) + '</a>'
		// });
		console.log(commentData[i]);
		if(commentData[i]['type']==2){
			var data = {id: commentData[i]['id'], content: commentData[i]['content'], totalUpvotes: commentData[i]['totalUpvotes']};
			var template = "<div class='commentDiv'><div id='{{id}}' class='commentVotingDiv'><button onclick='upvoteComment(this);' class='upvoteButton'>⇑</button><div class='upvotes'>{{totalUpvotes}}</div><button onclick='downvoteComment(this);' class='downvoteButton'>⇓</button></div><div class='content'>{{content}}</div></div>";
			var newRow = Mustache.to_html(template, data);
			$("#dontHateComments").append(newRow);
		}else if(commentData[i]['type']==3){
			var data = {id: commentData[i]['id'], content: commentData[i]['content'], totalUpvotes: commentData[i]['totalUpvotes']};
			var template = "<div class='commentDiv'><div id='{{id}}' class='commentVotingDiv'><button onclick='upvoteComment(this);' class='upvoteButton'>⇑</button><div class='upvotes'>{{totalUpvotes}}</div><button onclick='downvoteComment(this);' class='downvoteButton'>⇓</button></div><div class='content'>{{content}}</div></div>";
			var newRow = Mustache.to_html(template, data);
			$("#hateComments").append(newRow);
		}else if(commentData[i]['type']==4){
			var data = {id: commentData[i]['id'], content: commentData[i]['content'], totalUpvotes: commentData[i]['totalUpvotes']};
			var template = "<div class='commentDiv'><div id='{{id}}' class='commentVotingDiv'><button onclick='upvoteComment(this);' class='upvoteButton'>⇑</button><div class='upvotes'>{{totalUpvotes}}</div><button onclick='downvoteComment(this);' class='downvoteButton'>⇓</button></div><div class='content'>{{content}}</div></div>";
			var newRow = Mustache.to_html(template, data);
			$("#neutralInfo").append(newRow);
		}
	}
	$("div").linkify();
	$('#sidebar').linkify({
	    target: "_blank"
	});
});
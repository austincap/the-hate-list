  //upload and resize pics TIPR IS IN THERE TOO
  //HAS TO BE A BETTER WAY OF DOING THIS
  $( document ).ajaxComplete(function( ajaxEvent, request, settings ) {
      $(document).ready(function($){
        //FOR IMAGE UPLOAD
        var drop_items = $('.top-toolbar').find('.drop');
        //set up event listeners for the drop item
        function setUpEventListeners() {
          drop_items.each(function() {
            var thisDrop = $(this);
            thisDrop[0].addEventListener('dragenter', dragEnter);
            thisDrop[0].addEventListener('dragover', dragOver);
            thisDrop[0].addEventListener('dragleave', dragLeave);
            thisDrop[0].addEventListener('drop', drop);
          });
        }
        
        setUpEventListeners();
        //called as the draggable enters a droppable 
        //needs to return false to make droppable area valid
        function dragEnter(event) {
          //console.log('testdrag');
          var drop = this;
          $(drop).addClass('drop-active');
          event.preventDefault();
        }
        //called continually while the draggable is over a droppable 
        //needs to return false to make droppable area valid
        function dragOver(event) {
          //console.log('dragover');
          var drop = this;
          $(drop).addClass('drop-active');
          event.preventDefault();
        }
        //called when the draggable was inside a droppable but then left
        function dragLeave(event) {
          //console.log('dragLeave');
            var drop = this;
            $(drop).removeClass('drop-active');
        }
        //called when draggable is dropped on droppable 
        //final process, used to copy data or update UI on successful drop
        function drop(event) {
          console.log("DROP");
          event.stopPropagation();
          event.preventDefault();

          //main drop container
          drop = $(this);
          //remove class from drop zone
          drop.removeClass('drop-active');
          var dataList, dataText, dataType;
          //get the URL of elements being dragged here
          try {
            dataValue = event.dataTransfer.getData('text/html');
            //dataValue = event.dataTransfer.getData('text/uri-list');
            //dataType = 'text/uri-list';
            var regexVar = /HREF="([^\'\"]+)/g;
            var extractedLink = dataValue.match(regexVar)[0].slice(6);
          } catch (e) {
            //console.log("CATCH");
            dataValue = event.dataTransfer.getData('URL');
            console.log(dataValue);
            dataType = 'URL';
          }
          //if we have a URL passed
          if (dataValue) {
            //determine if our URL is an image
            imageDropped = false;
            var imageExtensions = ['.jpg', '.jpeg', '.png', '.bmp', '.gif'];
            for (i = 0; i < imageExtensions.length; i++) {
              if (dataValue.indexOf(imageExtensions[i]) !== -1) {
                //create our image from the URL of the drop
                var image = '<img src="' + dataValue + '">';
                drop.append(image);
                imageDropped = true;
                break;
              } else {
                if(extractedLink===undefined){
                  document.getElementById("chatinput").value = dataValue;
                }else{
                  document.getElementById("chatinput").value = extractedLink;
                }
                break;
              }
            }
            //if we dropped an image, notify user
            if (imageDropped == true) {
              //console.log('Successfully dropped from an online source.');
            } else {
              //console.log('Couldn\'t determine what was dropped, it didn\'t match expected image formats.');
            }
          }
          //if we have a list of files
          else {
            //check if any files were dropped (from the desktop etc)
            var dataFiles = event.dataTransfer.files;
            var dataOutput = [];
            if (dataFiles) {
              for (i = 0; i < dataFiles.length; i++) {
                var dataItem = dataFiles[i];
                var dataType = dataFiles[i].type;
                //check if this is an image
                if (dataType.match('image.*')) {
                  var dataLastModified = dataFiles[i].lastModified;
                  var dataLastModifiedDate = dataFiles[i].lastModifiedDate;
                  var dataName = dataFiles[i].name;
                  var dataSize = dataFiles[i].size;
                  var dataType = dataFiles[i].type;
                  //read into memory
                  var reader = new FileReader();
                  //when our image is loaded
                  reader.onload = (function(theFile) {
                    return function(e) {
                      var url = e.target.result;
                      document.getElementById('chatinput').value = url;
                      drop.append('<img onload="resizeimg(this,100,200);" src="' + url + '" title="' + dataName + '"/>');
                      console.log('Successfully dropped an image from your desktop!');
                    };
                  })(dataItem);
                  //load element
                  reader.readAsDataURL(dataItem);
                } else {
                  console.log('It looks like you have not dropped an image. Only images are allowed.');
                }
              }
            }
          }
        }

        $('.tip').tipr();
      });
  });

  function resizeimg(obj,maxW,maxH){
    var imgW=obj.width;
    var imgH=obj.height;
    if(imgW>maxW||imgH>maxH){       
      var ratioA=imgW/maxW;
      var ratioB=imgH/maxH;
      if(ratioA>ratioB){
        imgW=maxW;
        imgH=maxW*(imgH/imgW);
      } else{
        imgH=maxH;
        imgW=maxH*(imgW/imgH);
      }  
      obj.width=imgW;
      obj.height=imgH;
    }
  }

  //hide splash screen when you reload to change rooms
  if (sessionStorage.getItem('clientPlayerData') != null){
    console.log(sessionStorage.getItem('clientPlayerData'));
    if(JSON.parse(sessionStorage.getItem('clientPlayerData')).userID !== null){ logInUser(); }
    else{ logInAnon(); }
  }

  //subtitle function
  $(function(){
    $.getJSON('subtitles.json',function(data){
      $('#sub-title').append(data[util.getRandomInt(0,Object.keys(data).length)]['text']);
    });
  });

  function clickSelectItem(thisElement){
    $(".item_selected").removeClass("item_selected");
    $(thisElement).addClass("item_selected");
    var newSelectedItem = $(".item_selected").attr("id"); 
    if(newSelectedItem){
      playerManager.clientPlayerData.item = newSelectedItem;
    }
    this.sounds.placeblock.play();
    util.messageOverlay(playerManager.clientPlayerData.inventory[newSelectedItem]['item'] + ' equipped!', 900);
  }

  function useItem(){
    this.game.input.activePointer.middleButton.isDown = true;
    setTimeout(function(){
      this.game.input.activePointer.middleButton.isDown = false;
    }, 100)
  }

  function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
  }
  
  
  function logInAnon(){
    if(JSON.parse(sessionStorage.getItem('clientPlayerData')) != null){
      //if previous login was with a user id then it clears previous session
      if(JSON.parse(sessionStorage.getItem('clientPlayerData')).userID != null){
        //console.log('clearing prev session data');
        sessionStorage.clear();
      } else{
        //console.log('loading prev sesssion data');
        //playerManager.clientPlayerData = playerSession.clientPlayerData;
      }
    }
    document.getElementById("intro-div").style.display = "none";
    document.getElementById("login-div").style.display = "none";
    document.getElementById("register-div").style.display = "none";
    document.getElementById("open-menu-button").style.display = "inline";
    document.getElementById("open-tutorial").style.display = "inline";
    document.getElementById("grid").style.display = "grid";
    document.getElementById("game").style.display = "grid";
    var prevButtonColor = document.getElementById("more-info").style.background;
    document.getElementById("more-info").style.background = "white";
    setTimeout(function(){
    document.getElementById("more-info").style.background = prevButtonColor;
    }, 240);
    game.paused=false;
    loginButtonPushed=false;
    game.state.start('intro');
  }
  function logInUser(){
    //retaining password from room to room          
      if(sessionStorage.getItem('playerPassword')){
        var socketId = sessionStorage.getItem('clientPlayerData').origSocketId;
        var sessionSocketId = sessionStorage.getItem('origSocketId');
        if(sessionSocketId){
          socketId = sessionSocketId;
        }
        socket.emit('loginUser', {
          username: DOMPurify.sanitize(sessionStorage.getItem('playerUsername')), 
          password:DOMPurify.sanitize(sessionStorage.getItem('playerPassword')),
          origSocketId: socketId} );
      } else {
        socket.emit('loginUser', {
          username: DOMPurify.sanitize(document.getElementById('loginusername').value),
          password:DOMPurify.sanitize(document.getElementById('loginpassword').value), 
          origSocketId: socket.id} );
      }
    if(sessionStorage.getItem('clientPlayerData') != null){
      if(JSON.parse(sessionStorage.getItem('clientPlayerData')).userID !== null){
        //console.log('loading prev sesssion data');
        playerManager.clientPlayerData = JSON.parse(sessionStorage.getItem('clientPlayerData'));        
      } else{ //if there is no userID from previous session, clear previous sessionStorage
        //console.log('clearing prev session data');
        sessionStorage.clear();
      }
    }
    document.getElementById("intro-div").style.display = "none";
    document.getElementById("login-div").style.display = "none";
    document.getElementById("register-div").style.display = "none";
    document.getElementById("open-menu-button").style.display = "inline";
    document.getElementById("open-tutorial").style.display = "inline";
    document.getElementById("grid").style.display = "grid";
    document.getElementById("game").style.display = "grid";
    document.getElementById("accountObtained").style.display = "block";
    var prevButtonColor = document.getElementById("more-info").style.background;
    document.getElementById("more-info").style.background = "white";
    setTimeout(function(){
      document.getElementById("more-info").style.background = prevButtonColor;
    }, 240);
    game.paused=false;
    loginButtonPushed=true;
    game.state.start('intro');  
  }
  function registerUser(){
    socket.emit('addNewUserToDB', {username:document.getElementById('username').value, password:document.getElementById('password').value});
    document.getElementById("register-div").style.display = "none";
    document.getElementById("login-div").style.display = "block";
  }

  $('#agreeToTerms').click(function(){
    if($('#registerButton').is(':disabled')){$('#registerButton').removeAttr('disabled');} 
    else{$('#registerButton').attr('disabled','disabled');}
  });

  //CLOSE OVERLAYS ON DOUBLE CLICK
  $(document).dblclick(function(e){
    // if the target of the click isn't the container nor a descendant of the container
    var verse_container = $("#verse-creation-overlay");
    if ((!verse_container.is(e.target) && verse_container.has(e.target).length === 0) )  
    { verse_container.hide(); game.paused=false;playerManager.clientPlayerData.playerPaused=false;}
    // if the target of the click isn't the container nor a descendant of the container
    var comment_entry_container = $("#comment-entry-overlay");
    if ((!comment_entry_container.is(e.target) && comment_entry_container.has(e.target).length === 0) )  
    { comment_entry_container.hide(); game.paused=false;playerManager.clientPlayerData.playerPaused=false;}
    // if the target of the click isn't the container nor a descendant of the container
    var comment_reading_container = $("#comment-reading-overlay");
    if ((!comment_reading_container.is(e.target) && comment_reading_container.has(e.target).length === 0) )  
    { comment_reading_container.hide(); game.paused=false;playerManager.clientPlayerData.playerPaused=false;}
    var shunter_overlay_container = $("#shunter-overlay");
    if ((!shunter_overlay_container.is(e.target) && shunter_overlay_container.has(e.target).length === 0) )  
    { shunter_overlay_container.hide(); }
    var portalgun_overlay_container = $("#portalgun-overlay");
    if ((!portalgun_overlay_container.is(e.target) && portalgun_overlay_container.has(e.target).length === 0) )  
    { portalgun_overlay_container.hide(); }
    var edit_overlay_container = $("#edit-overlay");
    if ((!edit_overlay_container.is(e.target) && edit_overlay_container.has(e.target).length === 0) )  
    { edit_overlay_container.hide(); game.paused=false;playerManager.clientPlayerData.playerPaused=false;}
    var nomad_gif_container = $("#nomad-gif");
    if ((!nomad_gif_container.is(e.target) && nomad_gif_container.has(e.target).length === 0) )  
    { nomad_gif_container.hide(); game.paused=false;}
    var menu_overlay = $("#menu-overlay");
    if ((!menu_overlay.is(e.target) && menu_overlay.has(e.target).length === 0) )  
    { menu_overlay.removeClass('menu-rendered').empty(); game.paused=false;playerManager.clientPlayerData.playerPaused=false;}
    var youtubeIframe = $("#nomad-youtube-iframe");
    if ((!youtubeIframe.is(e.target) && youtubeIframe.has(e.target).length === 0) )  
    { youtubeIframe.hide(); }
    var newroom_password_entry_container = $("#password-protected-verse-overlay");
    if ((!newroom_password_entry_container.is(e.target) && newroom_password_entry_container.has(e.target).length === 0) )  
    { newroom_password_entry_container.hide(); }
  });
  //MAXIMIZE COMMENT READING OVERLAY
  $("#comment").on("click", function(){
    var comment_reader_container = $("#comment-reading-overlay");
    var comment = $("#comment");
    comment.removeClass("minified").addClass("maxified");
    comment_reader_container.removeClass("minified").addClass("maxified");
    comment_reader_container.css('top', '80px');
    comment_reader_container.css('left', '80px');
  });
  //CLOSE ALL OVERLAYS WITH ESC
  document.addEventListener("keyup", function(event){
    event.preventDefault();
    if (event.keyCode === 27){
      document.getElementById("login-div").style.display = "none";
      document.getElementById("register-div").style.display = "none";
      document.getElementById("comment-reading-overlay").style.display = "none";
      document.getElementById("comment-entry-overlay").style.display = "none";
      document.getElementById("verse-creation-overlay").style.display = "none";
      document.getElementById("shunter-overlay").style.display = "none";
      document.getElementById("edit-overlay").style.display = "none";
      document.getElementById("portalgun-overlay").style.display = "none";
      document.getElementById("nomad-gif").style.display = "none";
      $("#menu-overlay").removeClass('menu-rendered').empty();
      document.getElementById("nomad-youtube-iframe").style.display = "none";
      document.getElementById("password-protected-verse-overlay").style.display = "none";
      game.paused=false;
      playerManager.clientPlayerData.playerPaused=false;
    }
  });

  //TUTORIAL STUFF
  $("#tut-1").click(function() { return false; });
  $("#tut-2").click(function() { return false; });
  $("#tut-3").click(function() { return false; });
  $("#tut-4").click(function() { return false; });
  $("#tut-5").click(function() { return false; });
  $("#tut-6").click(function() { return false; });
  $("#tut-7").click(function() { return false; });
  $("#tut-8").click(function() { return false; });

  //CONTEXT MENU
    if(game.device.desktop){

    }else{

      var onlongtouch; 
      var timer, lockTimer;
      var touchduration = 800; //length of time we want the user to touch before we do something

      function touchstart(e){
        e.preventDefault();
        if(lockTimer){ return; }
        timer = setTimeout(onlongtouch, touchduration); 
        lockTimer = true;
      }

      function touchend(){
        if (timer){ clearTimeout(timer); lockTimer = false; }
      }

      onlongtouch = function(){
        document.getElementById("")
        // Show contextmenu
        $(".custom-menu").finish().toggle(100).css({
          top: event.pageY + "px",
          left: event.pageX + "px"
        });
      };


        window.addEventListener("touchstart", touchstart, false);
        window.addEventListener("touchend", touchend, false);

      // // If the document is clicked somewhere
      // $(document).bind("mousedown", function (e) {
      //   // If the clicked element is not the menu
      //   if (!$(e.target).parents(".custom-menu").length > 0) {
      //     // Hide it
      //     $(".custom-menu").hide(100);
      //   }
      // });

    }

      $(document).bind("contextmenu", function (event) {
        // Avoid the real one
        event.preventDefault();
        // Show contextmenu
        $(".custom-menu").finish().toggle(100).
        // In the right position (the mouse)
        css({
          top: event.pageY + "px",
          left: event.pageX + "px"
        });
      });
      
      // If the document is clicked somewhere
      $(document).bind("mousedown", function (e) {
        // If the clicked element is not the menu
        if (!$(e.target).parents(".custom-menu").length > 0) {
          // Hide it
          $(".custom-menu").hide(100);
        }
      });
      // If the menu element is clicked
      $(".custom-menu li").click(function(){
        // Hide it AFTER the action was triggered
        $(".custom-menu").hide(100);
      });
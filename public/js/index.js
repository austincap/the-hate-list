//var socket = io.connect("http://localhost:4000");
var socket = io.connect("http://159.203.169.144:4000");
socket.emit("getAllHatees");

function showNewPersonSubmissionContainer(){
	$("#newPersonSubmissionContainer").show();
}

function toAboutPage(){
	window.location.href = "/about.html";
}

function submitNewPerson(){
	$("#newPersonSubmissionContainer").hide();
	//var newPersonName = $("#newPersonName").val().slice(30);
	socket.emit("addNewPerson", $("#newPersonName").val());
	location.reload();
}

function voteHate(element){
	console.log($(element).attr('id'));
	socket.emit("voteHate", $(element).attr('id'));
	location.reload();
}

socket.on('heresYourHatees', function(hatees){
	console.log(hatees);
	for(i=0; i<hatees.length; i++){
		console.log(hatees[i]);
		var data = {id: hatees[i]['_id'], name: hatees[i]['name'], haters: hatees[i]['haters'], date: hatees[i]['date'], procount: hatees[i]['procount'], concount: hatees[i]['concount'], infocount: hatees[i]['infocount']};
		var template = "<tr><td>{{name}}</td><td class='haterstally'>{{haters}}<button id='{{id}}' onclick='voteHate(this);' class='votehate'>vote hate</button></td><td><a href='discussion.html#{{id}}'>{{concount}} comments from haters, {{procount}} comments against hate, {{infocount}} details</a></td><td>{{date}}</td></tr>";
		var newRow = Mustache.to_html(template, data);
		$("#hatelist tbody").append(newRow);
	}
});
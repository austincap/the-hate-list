//var config = require("dotenv").config();
var express = require("express");
var app = express();
var server = require("http").Server(app);
var io = require("socket.io")(server);
var session = require("express-session");
var sess = {
  secret: 'keyboard cat',
  cookie: {maxAge: 600000, expires: false}
};

const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");
var ObjectId = require("mongodb").ObjectID;
const url = "mongodb://hatedaemon:fucksluts10@ds061370.mlab.com:61370/thehatelist";
const dbName = "thehatelist";

var cookieIDs = [];

app.use(session(sess));

app.get('/', function(req, res, next){
	//res.sendFile(__dirname+"/public/index.html");
 //  if (req.session.views) {
 //    req.session.views++;
 //    console.log("num page views "+req.session.views);
 //    console.log("expires in "+req.session.cookie.maxAge / 1000 + "s");
 //    console.log(req.session.cookie);
 //    console.log(req.session.id);
 //  } else {
 //  	//ADD HATER FIRST TIME USER CONNECTS TO SERVER
	// const client = new MongoClient(url);
	// client.connect(function(err){
	// 	assert.equal(null, err);
	// 	var newHateeDocument = {
	// 		"type":1,
	// 		"haterid":req.session.id,
	// 		"votedfor":[]
	// 	};
	// 	const db = client.db(dbName);
	// 	db.collection('hhc').insertOne(newHateeDocument, function(err, r){
	// 		assert.equal(null, err);
	// 		assert.equal(1, r.insertedCount);
	// 		console.log("Hater added", req.session.id);
	// 	});
	// });
 //    req.session.views = 1;
 //    console.log('welcome to the session demo. refresh!');
 //    console.log(req.session.cookie);
 //  }
 //  cookieIDs.push(req.session.id);

 
  next();
});

app.use(express.static("public"));


io.on("connection", function(socket){
	//GET ALL HATEES
	socket.on("getAllHatees", function(){
		const client = new MongoClient(url);
		client.connect(function(err){
			const db = client.db(dbName);
			console.log('user connected');
			db.collection('hhc').find({"type":0}).sort({haters:-1}).toArray(function(err, hatees){
				assert.equal(err, null);
				console.log("Found the following records");
				var newHatees = hatees.map(obj =>{
					var newObj = {};
					newObj = obj;
					newObj['date'] = String(obj['_id'].getTimestamp()).slice(0,15);
					newObj['procount'] = obj['pro'].length;
					newObj['concount'] = obj['con'].length;
					newObj['infocount'] = obj['info'].length;
					return newObj;
				});
				console.log(newHatees);
				socket.emit('heresYourHatees', newHatees);
			});
		});		
	});
	//ADD HATEE
	socket.on("addNewPerson", function(personName){
		const client = new MongoClient(url);
		client.connect(function(err){
			assert.equal(null, err);
			var newHateeDocument = {
				"type":0,
				"haters":1,
				"name":personName,
				"pro":[],
				"con":[],
				"info":[]
			};
			const db = client.db(dbName);
			db.collection('hhc').insertOne(newHateeDocument, function(err, r){
				assert.equal(null, err);
				assert.equal(1, r.insertedCount);
				console.log("Hatee added", personName);
			});
		});
	});
	//VOTE HATE
	socket.on("voteHate", function(id){
		//console.log(req.session.id);
		const client = new MongoClient(url);
		client.connect(function(err){
			const db = client.db(dbName);
			console.log('user voted');
			console.log(id);
			// db.collection('hhc').find({haterid: req.session.id}, function(err, res){
			// 	console.log('ID FOUND');
			// 	console.log(req.session.id);
			// });
			db.collection('hhc').findOneAndUpdate( {_id: ObjectId(id)}, {$inc:{haters:1}}, {returnNewDocument:true}, function(err, res){
				console.log(JSON.stringify(res));
			});
		});
	});
	//ADD NEW COMMENT
	socket.on("newComment", function(commentData){
		const client = new MongoClient(url);
		client.connect(function(err){
			assert.equal(null, err);
			var newComment = {
				"type":commentData.commentType,
				"content":commentData.content,
				"upvotes":1,
				"downvotes":0,
				"hateeId":commentData.hateeId
			};
			const db = client.db(dbName);
			db.collection('hhc').insertOne(newComment, function(err, commentRes){
				assert.equal(null, err);
				assert.equal(1, commentRes.insertedCount);
				console.log(commentRes.ops[0]['_id']);
				if(commentData.commentType==2){
					db.collection('hhc').findOneAndUpdate( {_id: ObjectId(commentData.hateeId)}, {$push:{pro:commentRes.ops[0]['_id']}}, function(err, r){
						console.log("added to hatees pro comment array");
					});
				}else if(commentData.commentType==3){
					db.collection('hhc').findOneAndUpdate( {_id: ObjectId(commentData.hateeId)}, {$push:{con:commentRes.ops[0]['_id']}}, function(err, r){
						console.log("added to hatees con comment array");
					});
				}else if(commentData.commentType==4){
					db.collection('hhc').findOneAndUpdate( {_id: ObjectId(commentData.hateeId)}, {$push:{info:commentRes.ops[0]['_id']}}, function(err, r){
						console.log("added to hatees info comment array");
					});
				}
			});
		});
	});
	//UPVOTE OR DOWNVOTE COMMENT
	socket.on("voteComment", function(commentVoteData){
		const client = new MongoClient(url);
		client.connect(function(err){
			const db = client.db(dbName);
			console.log('user voted');
			if(commentVoteData.upOrDown===1){
				db.collection('hhc').findOneAndUpdate( {_id: ObjectId(commentVoteData.commentID)}, {$inc:{upvotes:1}}, {returnNewDocument:true}, function(err, res){
					console.log(JSON.stringify(res));
				});	
			}else{
				db.collection('hhc').findOneAndUpdate( {_id: ObjectId(commentVoteData.commentID)}, {$inc:{downvotes:1}}, {returnNewDocument:true}, function(err, res){
					console.log(JSON.stringify(res));
				});	
			}
		});
	});
	//GET ALL COMMENTS
	socket.on("getAllComments", function(hateeId){
		const client = new MongoClient(url);
		client.connect(function(err){
			const db = client.db(dbName);
			console.log('user accessed comments for', hateeId);
			db.collection('hhc').find({hateeId: hateeId}).toArray(function(err, results){
				assert.equal(err, null);
				console.log("Found the following records");
				console.log(results);
				var allCommentsPackage = results.map(result =>{
					var newObj = {
						id: result['_id'],
						type: result['type'],
						content: result['content'],
						totalUpvotes: result['upvotes']-result['downvotes']
					};
					return newObj;
				});
				socket.emit('heresYourComments', allCommentsPackage);
				db.collection('hhc').find({_id: ObjectId(hateeId)}).toArray(function(err, results){
					assert.equal(err, null);
					console.log("Found the following records");
					console.log(results);
					socket.emit('heresYourPersonName', {personName:results[0]['name'], haterCount:results[0]['haters']});
				});
			});
		});
	});
});



server.listen(4000);